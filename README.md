# MXit-forum

** Last Update: 2012-11-13 **

Mxit-forum is an open-source MXit forum system. Please see wiki for more info: [https://bitbucket.org/carelvwyk/mxit-forum/wiki/Home](https://bitbucket.org/carelvwyk/mxit-forum/wiki/Home)

# Steps for getting up and running:

* These steps are just a guideline, do whatever you find most comfortable.
* In your editor set up a vertical ruler at 100 characters to serve as horizontal boundary for code blocks.

## Install [rbenv](https://github.com/sstephenson/rbenv):

$ cd

$ git clone git://github.com/sstephenson/rbenv.git .rbenv

**For bash:**

$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile

$ echo 'eval "$(rbenv init -)"' >> ~/.bash_profile

$ exec $SHELL

**For zsh:**

$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshenv

$ echo 'eval "$(rbenv init -)"' >> ~/.zshenv

$ exec $SHELL

## Install [Ruby-build](https://github.com/sstephenson/ruby-build):

$ git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

## Install Ruby:

$ rbenv install 1.9.3-p194

## Install Ruby Gems:

$ cd ~/your/projects/directory

$ git clone git://github.com/rubygems/rubygems.git

$ cd rubygems

$ sudo ruby setup.rb

## Install Rails:

$ gem install rails

## Install MongoDB:

Find your own way

## Install Heroku:

[https://toolbelt.heroku.com/](https://toolbelt.heroku.com/)

Use the mxitforum app by logging in with the talkinc account or obtaining a collaborator invite from Carel

### Add heroku repo as remote head:

$ heroku git:remote -a mxitforum -r mxitforum

## Clone repository:

$ cd ~/your/projects/directory

$ git clone git@bitbucket.org:carelvwyk/mxit-forum.git

## Install Gems using bundle:

In the project's directory, run "bundle" (remember to run it every time you update the Gemfile)

$ bundle


# Other Hints:

## Deploying to Heroku:

$ git push [heroku app name] [branch name]:master

e.g.

$ git push mxitforum heroku:master

## Pushing a new branch to bitbucket:

$ git push -u origin [branch name]

e.g.

$ git push -u origin new_feature