# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

new_thread = ForumThread.create(:title => 'Test Thread')
new_user = User.create(:auth_key => '123', :auth_provider => :mxit)

(0..21).each do |num|
  new_post = Post.create(:text => "Post #{num}...", :user => new_user,
    :forum_thread => new_thread)
end
