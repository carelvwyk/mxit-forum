require 'digest/sha1'

class Conversation
  include Mongoid::Document
  # Fields
  field :lookup_hash,           type: String
  field :created,               type: DateTime, default: -> { Time.now }
  field :last_message_time,     type: DateTime, default: -> { Time.now }
  # Array of user ids of users that have read all messages in this conversation
  field :last_message_seen_by,  type: Array,    default: []

  # Relationships
  embeds_many :messages
  has_and_belongs_to_many :participants, :class_name => 'User'

  # Constraints
  validates_presence_of :lookup_hash

  # Indexes
  index({ lookup_hash: 1 }, { unique: true, name: "lookup_hash_index" })
    # Used to show a user a list of conversations ordered by last_message_time
  index({ _id: 1, last_message_time: -1 }, { unique: true, name: "id_last_message_time_index" })

  # Class Methods
  def self.get_conversation(participants)
    participant_ids = participants.map{|p| p.id}
    Conversation.find_or_create_by(
      :lookup_hash => get_lookup_hash(participant_ids)) do |c|
        c.participants = participants
      end
  end

  # Instance Methods
  def add_message(message)
    # TODO: Limit max number of messages per conversation
    self.messages << message
    self.last_message_time = Time.now
    self.last_message_seen_by.clear

    # Mark as seen by author
    self.last_message_seen_by << message.author.id

    # Set "new message" flag for other participants
    self.get_other_participants(message.author).each do |participant|
      participant.has_new_messages = true
      participant.save
    end
    self.save
  end

  def mark_as_seen(user)
    self.last_message_seen_by << user.id
    self.save
  end

  def get_other_participants(user)
    self.participants.select{|p| p != user}
  end

  def get_other_participant_names(user)
    self.participants.map { |p| p.auth_key if p != user}.compact
  end

  def contains_unread?(user)
    not self.last_message_seen_by.include?(user.id)
  end

  def authorized_viewer?(user)
    self.participants.include?(user)
  end

  private

    def self.get_lookup_hash(participant_ids)
      lookup_key = participant_ids.sort.join(':')
      Digest::SHA1.hexdigest lookup_key
    end

end
