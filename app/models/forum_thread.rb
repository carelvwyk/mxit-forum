class ForumThread
  include Mongoid::Document
  # Fields
  field :title,       type: String
  field :created,     type: DateTime, default: -> { Time.now }
  field :last_post,   type: DateTime
  field :post_count,  type: Integer,  default: 0

  # Constraints
  validates_presence_of :title

  # Relationships
  has_many :posts
end
