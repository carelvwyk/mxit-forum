class Administrator
  include Mongoid::Document

  field :email,  type: String

  index({ email: 1 }, { unique: true, name: "email_index" })
end