require 'zlib'

class User
  include Mongoid::Document
  # Fields
  # Providers:  :mxit, :google
  field :auth_provider,     type: Symbol
  field :auth_key,          type: String

  #field :mxit_id, type: String
  field :joined,            type: DateTime, default: ->{ Time.now }
  field :last_activity,     type: DateTime, default: -> { Time.now }
  field :visit_count,       type: Integer,  default: 1
  field :post_count,        type: Integer,  default: 0
  field :has_new_messages,  type: Boolean,  default: false

  # Constraints
  validates_presence_of :auth_provider
  validates_presence_of :auth_key

  # Relationships
  has_many :posts
  has_and_belongs_to_many :conversations

  # Indexes
  index({ auth_key: 1 }, { unique: true, name: "auth_key_index" })

  # Instance Methods

  # Calculate the Google Analytics utma code for this user
  def ga_utma(domain, session_created)
    domain_hash = Zlib.crc32 domain
    visitor_id = Zlib.crc32 self.auth_key
    initial_session = self.joined.to_i
    last_session = session_created.to_i
    current_session = session_created.to_i
    visit_count = self.visit_count
    "1.#{visitor_id}.#{initial_session}.#{last_session}.#{current_session}.#{visit_count}"
  end
end
