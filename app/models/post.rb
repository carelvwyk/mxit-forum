class Post
  include Mongoid::Document
  # Fields
  field :created, type: DateTime, default: -> { Time.now }
  field :text,    type: String

  # Relationships
  belongs_to :user
  belongs_to :forum_thread

  # Constraints
  validates_length_of :text, minimum: 2
  validates_presence_of :user
  validates_presence_of :forum_thread

  # Indexes
  index({ created: -1 }, { unique: false, name: "created_index" })

  # Instance Methods
  def update_parents
    self.user.post_count += 1
    self.forum_thread.post_count += 1
    self.forum_thread.last_post = Time.now
    self.save
  end
end
