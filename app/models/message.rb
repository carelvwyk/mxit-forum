class Message
  include Mongoid::Document
  # Fields
  field :created, type: DateTime, default: -> { Time.now }
  field :text,    type: String

  # Relationships
  embedded_in :conversation
  belongs_to :author, :class_name => 'User'

  # Constraints
  validates_length_of :text, minimum: 2, maximum: 256
  validates_presence_of :author

  def sent_today?
    self.created.to_date == Date.today
  end

  def sent_by?(user)
    self.author == user
  end
end
