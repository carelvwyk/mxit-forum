module ResqueJobs
  require 'heroku_resque_auto_scale.rb'
  class TestJob
    extend HerokuAutoScaler::AutoScaling

    @queue = :test_job

    def self.perform(test_parameter)
      Rails.logger.info "Testing a test job"
    rescue Resque::TermException # Used to handle worker termination via SIGTERM
      Resque.enqueue(self, key) # Re-enqueue job via another worker
    end
  end

  def self.async_test_job(test_parameter)
    Resque.enqueue(TestJob, test_parameter)
  end
end