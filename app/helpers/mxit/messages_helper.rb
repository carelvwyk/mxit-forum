module Mxit::MessagesHelper
  def other_participant_link (user, conversation)
    other_participant = conversation.get_other_participants(user)[0] || user
    link_to other_participant.auth_key, mxit_profile_path(other_participant)
  end
end