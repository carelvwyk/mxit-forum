class Admin::AdminController < ApplicationController
  before_filter :is_admin

  def is_admin
    Administrator.find_by(:email => session[:email])
  end
end