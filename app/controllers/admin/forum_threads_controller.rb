class Admin::ForumThreadsController < Admin::AdminController
  def index
    render :status => 200, :text => "OK"
  end

  def new
    @forum_thread = ForumThread.new()
  end
end
