class Mxit::ProfilesController < Mxit::MxitController
  before_filter :assign_profile

  def show
  end

  def send_message
    # Create new conversation
    if @user == @profile
      redirect_to :action => :show # Don't let a user send a message to himself
      return
    end

    participants = [@user, @profile]
    redirect_to new_mxit_conversation_message_path(Conversation.get_conversation(participants))
  end

  private
    def assign_profile
      @profile = User.find(params[:id]) if params[:id]
      raise "Profile not found" if @profile.nil?
    end
end