class Mxit::MessagesController < Mxit::MxitController
  before_filter :assign_conversation

  def new
    @message = Message.new()
  end

  def create
    @message = Message.new(params[:message].merge(:author_id => @user.id))
    if @message.valid?
      @conversation.add_message(@message)
    else
      render :new
      return
    end
  end

  private
    def assign_conversation
      @conversation = Conversation.find(params[:conversation_id]) if params[:conversation_id]
      raise "Conversation not found" if @conversation.nil?
    end
end