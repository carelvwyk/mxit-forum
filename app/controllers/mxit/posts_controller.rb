class Mxit::PostsController < Mxit::MxitController
  before_filter :assign_thread

  def index
    @page = params[:page] || 1 # 1 based index
    @page = @page.to_i
    @limit = 10
    skip = (@page-1)*@limit # page is 1 based index
    # limit + 1 so we know whether to draw a "next page" button or not.
    # to_a to avoid .length returning the full number of posts returned.
    @posts = @thread.posts.desc(:created).skip(skip).limit(@limit+1).to_a

    # TODO: Jump to first or last links
  end

  def new
    @post = Post.new()
  end

  def create
    @post = @thread.posts.new(params[:post].merge(:user_id => @user.id))
    if @post.save
      @post.update_parents
      redirect_to mxit_forum_thread_posts_path(@thread)
    else
      render :new
    end
  end

  private
    def assign_thread
      @thread = ForumThread.find(params[:forum_thread_id]) if params[:forum_thread_id]
      raise "Thread not found" if @thread.nil?
    end
end