class Mxit::ConversationController < Mxit::MxitController
  def index
    @conversations = @user.conversations.desc(:last_message_time)
    # When the user access his inbox, clear the "has_new_messages" flag
    @user.has_new_messages = false
    @user.save
  end

  def show
    id = params[:id]
    # Make sure the logged in user has access to this conversation
    @conversation = Conversation.find(id)
    if not @conversation.authorized_viewer?(@user)
      redirect_to mxit_path() # Mxit users root path
      return
    end

    @conversation.mark_as_seen(@user) # Mark conversation as seen by logged in user
  end
end