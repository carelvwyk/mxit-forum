require "resque/server"

MxitForum::Application.routes.draw do
  root :to => 'mxit::welcome#index'

  # ++ Mxit Interface ++
  namespace :mxit do
    match '/welcome/:action' => 'welcome#'

    resources :forum_threads, :only => [] do
      resources :posts, :only => [:index, :new, :create]
    end

    resources :profiles, :only => [:show] do
        member do
          get :send_message
        end
    end

    # Conversations are created when new messages are sent
    resources :conversation, :only => [:index, :show] do
      resources :messages, :only => [:new, :create]
    end

    match 'inbox' => 'conversation#index'
  end
  # -- Mxit Interface --

  # ++ Admin ++
  #namespace :admin do
  #  resources :threads
  #end
  # -- Admin --

  # ++ Resque ++
  mount Resque::Server, :at => 'admin/resque'
  # -- Resque --
end
